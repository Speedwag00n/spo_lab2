#!/usr/bin/env escript

-import(partions, [show_partitions/0]).
-import(fs_explorer, [explore_fs/1]).

main([]) ->
    io:format("Show disks and partitions.~n"),
    show_partitions();

main(Args) when (tl(Args) == []) ->
    io:format("Explore specified FS~n"),
    explore_fs(hd(Args));
    
main(Args) ->
    io:format("To many arguments~n").
