-module(fs_explorer).
-export([explore_fs/1]).
-import(string, [sub_word/2, words/1, str/2]).
-import(lists, [delete/2]).
-import(fat32, [open_fs/1, close_fs/1, show_current_dir/1, move_to_directory/2, show_content/2, copy_from_fs/3]). 

explore_fs(Args) ->
    try
        StructsAddress = open_fs(Args),
        shell_loop(StructsAddress)
    catch
        _:_ ->
            io:format("Could not open specified disk/partition/file as FAT32 file system~n")
    end.


shell_loop(StructsAddress) ->
    Line = io:get_line("# "),
    Command = sub_word(delete($\n, Line), 1),
    
    case Command of
        "ls" ->
            execute_ls(StructsAddress, Line),
            shell_loop(StructsAddress);
        "cp" ->
            execute_cp(StructsAddress, Line),
            shell_loop(StructsAddress);
        "cd" ->
            execute_cd(StructsAddress, Line),
            shell_loop(StructsAddress);
        "pwd" ->
            execute_pwd(StructsAddress),
            shell_loop(StructsAddress);
        "help" ->
            execute_help(), 
            shell_loop(StructsAddress);
        "exit" ->
            close_fs(StructsAddress),
            io:format("Exiting...~n"),
            halt();
        "" ->
            %% Empty line. Do nothing
            shell_loop(StructsAddress);
        _ ->
            io:format("You put unknown command. Type \"help\" for list all existing commands~n"),
            shell_loop(StructsAddress)
    end.

execute_ls(StructsAddress, Line) ->
    WordsNumber = words(Line),
    if
        WordsNumber == 1 ->
            try
                show_content(StructsAddress, "")
            catch
                _:_ ->
                    io:format("Could not show content of specified directory~n")
            end;
        WordsNumber == 2 ->
            TargetPath = sub_word(delete($\n, Line), 2),
            try
                show_content(StructsAddress, TargetPath)
            catch
                _:_ ->
                    io:format("Could not show content of specified directory~n")
            end;
        true ->
            io:format("You must specify destination path as parameters of 'cd' command~n")
    end.

execute_cp(StructsAddress, Line) ->
    WordsNumber = words(Line),
    if
        WordsNumber == 3 ->
            Source = sub_word(delete($\n, Line), 2),
            Destination = sub_word(delete($\n, Line), 3),
            DestinationSlashPosition = str(Destination, "/"),
            if
                DestinationSlashPosition == 1 ->
                    try
                        ReturnCode = copy_from_fs(StructsAddress, Source, Destination),
                        if
                            ReturnCode == 0 ->
                                true;
                            ReturnCode == 1 ->
                                io:format("Could not find file specified by source path~n");
                            true ->
                                io:format("Unknown error~n")
                        end
                    catch
                        _:_ ->
                            io:format("Could not copy files~n")
                    end;
                true ->
                    io:format("Specify destination path as an absolute path~n")
            end;
        true ->
            io:format("You must specify source path and destination path as parameters of 'cp' command~n")
    end.

execute_cd(StructsAddress, Line) ->
    WordsNumber = words(Line),
    if
        WordsNumber == 2 ->
            TargetPath = sub_word(delete($\n, Line), 2),
            try
                ReturnCode = move_to_directory(StructsAddress, TargetPath),
                if
                    ReturnCode == 1 ->
                        io:format("Could not find specified directory~n")
                end
            catch
                _:_ ->
                    io:format("Could not find specified directory~n")
            end;
        true ->
            io:format("You must specify destination path as parameters of 'cd' command~n")
    end.

execute_pwd(StructsAddress) ->
    show_current_dir(StructsAddress).

execute_help() ->
    io:format("Existing commands:~n"),
    io:format(" ls - print list of files and directories in current directory~n"),
    io:format(" cp - copy files from target file system~n"),
    io:format(" cd - change current directory~n"),
    io:format(" pwd - show path to current directory~n"),
    io:format(" help - print this information~n"),
    io:format(" exit - close this program~n").
