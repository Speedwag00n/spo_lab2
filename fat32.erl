-module(fat32).
-export([open_fs/1, close_fs/1, show_current_dir/1, move_to_directory/2, show_content/2, copy_from_fs/3]).
-on_load(init/0).

init() ->
    ok = erlang:load_nif("./fat32_nif", 0).

open_fs(_X) ->
    exit(nif_library_not_loaded).

close_fs(_X) ->
    exit(nif_library_not_loaded).

show_current_dir(_X) ->
    exit(nif_library_not_loaded).

move_to_directory(_X, _Y) ->
    exit(nif_library_not_loaded).

show_content(_X, _Y) ->
    exit(nif_library_not_loaded).

copy_from_fs(_X, _Y, _Z) ->
    exit(nif_library_not_loaded).
