-module(partions).
-export([show_partitions/0]).
-import(string, [concat/2, str/2]). 

-define(SYS_BLOCK_PATH, "/sys/block/").

show_partitions() ->
    {ok, FileNames} = file:list_dir(?SYS_BLOCK_PATH),
    lists:foreach(fun(DiskName) ->
        print_disk_info(DiskName)
    end, FileNames).

print_disk_info(DiskName) ->
    io:format(concat(concat("/dev/", DiskName), "\n")),
    {ok, PartitionsName} = file:list_dir(concat(?SYS_BLOCK_PATH, DiskName)),
    lists:foreach(fun(PartitionName) ->
    	Position = str(PartitionName, DiskName),
        if
            Position == 1 -> io:format(concat(concat("- /dev/", PartitionName), "\n"));
            true -> false
        end
    end, PartitionsName).
