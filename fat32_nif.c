#include <erl_nif.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "fat32_consts.c"
#include "fat32.h"

struct fs_structures *get_structure(ErlNifEnv* env, const ERL_NIF_TERM argv[]) {
    long structs_pointer = 0;
    enif_get_int64(env, argv[0], &structs_pointer);
    return (struct fs_structures*) structs_pointer;
}

static ERL_NIF_TERM open_fs_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    char target_path[MAX_PATH_LENGTH] = {0};
    char path_received = enif_get_string(env, argv[0], target_path, MAX_PATH_LENGTH, ERL_NIF_LATIN1);

    if (!path_received)
    {
        return enif_make_badarg(env);
    }

    struct fs_structures *fs_structures = open_fs(target_path);

    if (fs_structures == NULL)
    {
        return enif_make_badarg(env);
    }
    else
    {
        return enif_make_int64(env, (long)fs_structures);
    }
}

static ERL_NIF_TERM close_fs_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    struct fs_structures *structure = get_structure(env, argv);
    close_fs(structure);
    structure = NULL;
    return enif_make_int(env, 0);
}

static ERL_NIF_TERM show_current_dir_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    struct fs_structures *structure = get_structure(env, argv);
    printf("%s\n", structure->current_path);
    return enif_make_int(env, 0);
}

static ERL_NIF_TERM move_to_directory_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    struct fs_structures *structure = get_structure(env, argv);

    char target_path[MAX_PATH_LENGTH] = {0};
    char path_received = enif_get_string(env, argv[1], target_path, MAX_PATH_LENGTH, ERL_NIF_LATIN1);

    if (!path_received)
    {
        return enif_make_badarg(env);
    }

    return enif_make_int(env, move_to_directory(structure, target_path));
}

static ERL_NIF_TERM show_content_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    struct fs_structures *structure = get_structure(env, argv);

    char path[MAX_PATH_LENGTH] = {0};
    char path_received = enif_get_string(env, argv[1], path, MAX_PATH_LENGTH, ERL_NIF_LATIN1);

    if (!path_received)
    {
        return enif_make_badarg(env);
    }

    struct file *file;
    if (!strcmp("", path))
    {
        file = read_directory(structure, structure->current_cluster);
    }
    else if (path[0] == '/')
    {
        struct file *first_directory = read_directory(structure, structure->boot_sector_structure->root_cluster_number);  
        file = find_directory(structure, path + 1, first_directory);
        if (file != first_directory) {
            free(first_directory);
        }
    }
    else
    {
        struct file *first_directory = read_directory(structure, structure->current_cluster);
        file = find_directory(structure, path, first_directory);
        if (file != first_directory) {
            free(first_directory);
        }
    }

    if (file != NULL)
    {
        struct file *current_file = file;
        while (current_file != NULL) {
            if (current_file->is_directory) {
                printf("%s/\n", current_file->file_name);
            } else {
                printf("%s\n", current_file->file_name);
            }
            current_file = current_file->next_file;
        }
        free_file(file);

        return enif_make_int(env, 0);
    }
    else
    {
        return enif_make_int(env, 1);
    }
}

void copy_file(struct fs_structures *structures, char *target, struct file *file)
{
    int directory_specified = target[strlen(target) - 1] == '/' ? 1 : 0;

    char path[MAX_PATH_LENGTH] = "";
    strcat(path, target);
    if (directory_specified)
    {
        strcat(path, file->file_name);
    }

    int target_file_descriptor = open(path, O_RDWR | O_CREAT, 0777);

    unsigned int current_cluster = file->first_cluster;
    unsigned int cluster_size = structures->boot_sector_structure->bytes_per_sector * structures->boot_sector_structure->sectors_per_cluster;
    char *cluster_buffer = malloc(cluster_size);

    unsigned long bytes_left = file->size;

    while (bytes_left)
    {
        unsigned long int bytes_to_read = bytes_left < cluster_size ? bytes_left : cluster_size;
        bytes_left -= bytes_to_read;
        read_cluster(structures, current_cluster, cluster_buffer);
        current_cluster = read_table_address(structures, current_cluster);
        write(target_file_descriptor, cluster_buffer, bytes_to_read);
    }

    free(cluster_buffer);
    close(target_file_descriptor);
}

void copy_dir(struct fs_structures *fs_structures, char *destination, struct file *directory)
{
    mkdir(destination, 0777);
    struct file *file = read_directory(fs_structures, directory->first_cluster);
    struct file *current_file = file;
    while (current_file != NULL)
    {
        char subdirectory_path[MAX_PATH_LENGTH] = "";
        strcat(subdirectory_path, destination);
        strcat(subdirectory_path, "/");
        strcat(subdirectory_path, current_file->file_name);

        if (current_file->is_directory)
        {
            if (!strcmp(current_file->file_name, ".") || !strcmp(current_file->file_name, ".."))
            {
                current_file = current_file->next_file;
                continue;
            }
            copy_dir(fs_structures, subdirectory_path, current_file);
        }
        else
        {
            copy_file(fs_structures, subdirectory_path, current_file);
        }
        current_file = current_file->next_file;
    }
    free_file(file);
}

static ERL_NIF_TERM copy_from_fs_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    struct fs_structures *structure = get_structure(env, argv);

    char source[MAX_PATH_LENGTH] = {0};
    char path_received = enif_get_string(env, argv[1], source, MAX_PATH_LENGTH, ERL_NIF_LATIN1);

    if (!path_received)
    {
        return enif_make_badarg(env);
    }

    char destination[MAX_PATH_LENGTH] = {0};
    path_received = enif_get_string(env, argv[2], destination, MAX_PATH_LENGTH, ERL_NIF_LATIN1);

    if (!path_received)
    {
        return enif_make_badarg(env);
    }

    int target_found = 0;

    unsigned int last_subpath_slash = strlen(source);
    while (source[last_subpath_slash] != '/' && last_subpath_slash != 0)
    {
        last_subpath_slash--;
    }

    char path[MAX_PATH_LENGTH] = {0};
    char file_name[MAX_PATH_LENGTH] = {0};

    strncpy(path, source, last_subpath_slash);
    if (last_subpath_slash == 0 && source[0] != '/')
    {
        strncpy(file_name, source + last_subpath_slash, strlen(source) - last_subpath_slash);
    }
    else
    {
        strncpy(file_name, source + last_subpath_slash + 1, strlen(source) - last_subpath_slash);
    }

    struct file *current_file;
    if (path[0] == '/')
    {
        struct file *first_directory = read_directory(structure, structure->boot_sector_structure->root_cluster_number);  
        current_file = find_directory(structure, path + 1, first_directory);
        if (current_file != first_directory)
        {
            free(first_directory);
        }
    }
    else
    {
        struct file *first_directory = read_directory(structure, structure->current_cluster);
        current_file = find_directory(structure, path, first_directory);
        if (current_file != first_directory)
        {
            free(first_directory);
        }
    }

    while (current_file != NULL)
    {
        if (!strcmp(current_file->file_name, file_name))
        {
            if (current_file->is_directory)
            {
                copy_dir(structure, destination, current_file);
            }
            else
            {
                copy_file(structure, destination, current_file);
            }

            target_found = 1;

            break;
        }

        current_file = current_file->next_file;
    }

    if (!target_found)
    {
        return enif_make_int(env, 1);
    }

    free_file(current_file);

    return enif_make_int(env, 0);
}


static ErlNifFunc functions[] = {
    {"open_fs", 1, open_fs_nif},
    {"close_fs", 1, close_fs_nif},
    {"show_current_dir", 1, show_current_dir_nif},
    {"move_to_directory", 2, move_to_directory_nif},
    {"show_content", 2, show_content_nif},
    {"copy_from_fs", 3, copy_from_fs_nif}
};

ERL_NIF_INIT(fat32, functions, NULL, NULL, NULL, NULL);
