all: lib erlang

lib:
	gcc -o fat32_nif.so -fpic -shared fat32.c fat32_nif.c

erlang:
	erlc partions.erl
	erlc fs_explorer.erl
	erlc fat32.erl
